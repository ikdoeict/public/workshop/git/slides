# Git workshop slidedeck

## Getting started
- 🚀 [Install reveal.js](https://revealjs.com/installation)
- 👀 [View the demo presentation](https://revealjs.com/demo)
- 📖 [Read the documentation](https://revealjs.com/markup/)
- 🖌 [Try the visual editor for reveal.js at Slides.com](https://slides.com/)
- 🎬 [Watch the reveal.js video course (paid)](https://revealjs.com/course)

## Extra

- Created an Odisee theme
- Added some helper classes for margins, paddings and text: m-t-\*, p-t-\*, text-*, etc.


